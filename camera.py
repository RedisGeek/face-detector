import cv2
import numpy as np

faceCascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
eyeCascade = cv2.CascadeClassifier('haarcascade_eye.xml')
smileCascade = cv2.CascadeClassifier('haarcascade_smile.xml')

cap = cv2.VideoCapture(0)
cap.set(3,640)
cap.set(4,480)

while(True):
	# ret, frame = cap.read()
	ret, img = cap.read()
	img = cv2.flip(img, 1)
	# frame = cv2.flip(frame, -1)
	# gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
	gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

	# Detecter face
	
	faces = faceCascade.detectMultiScale(
		gray,
		scaleFactor=1.3,
		minNeighbors=5,
		minSize=(30, 30)
	)

	for (x,y,w,h) in faces:
		cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)
		cv2.putText(img, 'Visage', (x,y-10), cv2.FONT_HERSHEY_SIMPLEX, 0.9, (36,255,12),2)
		roi_gray = gray[y:y+h, x:x+w]
		roi_color = img[y:y+h, x:x+w]
	
	# roi_gray = gray[y:y+h, x:x+w]

		eyes = eyeCascade.detectMultiScale(
			roi_gray,
			scaleFactor= 1.5,
			minNeighbors=5,
			minSize=(5,5),
		)

		for (ex , ey , ew, eh) in eyes:
			cv2.rectangle(roi_color, (ex , ey), (ex + ew, ey + eh), (0,255,0), 2)
			cv2.putText(img, 'Yeux', (ex,ey-10), cv2.FONT_HERSHEY_SIMPLEX, 0.9, (36,255,12),2)


		smile = smileCascade.detectMultiScale(
			roi_gray,
			scaleFactor= 1.5,
			minNeighbors=15,
			minSize=(25,25)
		)

		for (xx , yy , ww , hh) in smile:
			cv2.rectangle(roi_color, (xx , yy), (xx + ww, yy + hh), (0, 0, 255), 2)
			cv2.putText(img, 'Sourire', (xx,yy-10), cv2.FONT_HERSHEY_SIMPLEX, 0.9, (36,255,12),2)		

	# Test webcam
	# cv2.imshow('frame', frame)
	# cv2.imshow('gray',frame)

	cv2.imshow('Capture',img)

	k = cv2.waitKey(30) & 0xff
	if k == 27:
		break

cap.release()
cap.destroyAllWindows()	